@file:Suppress("DuplicatedCode")

package net.krazyweb.util

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.io.ByteArrayOutputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import java.util.zip.GZIPInputStream

private val log: Logger = LogManager.getLogger("HttpUtils")

data class Response(val code: Int, val body: String, val cookies: String?)
data class RawResponse(val code: Int, val body: ByteArray, val cookies: String?)

fun get(url: String, cookieString: String? = null, additionalHeaders: Map<String, String>? = null): Response {

	val connection = URL(url).openConnection() as HttpURLConnection
	connection.setRequestProperty("Accept-Encoding", "gzip")

	additionalHeaders?.entries?.forEach {
		connection.setRequestProperty(it.key, it.value)
	}

	if (cookieString != null) {
		connection.setRequestProperty("Cookie", cookieString)
	}

	return if (connection.responseCode != 404 && connection.responseCode != 403) {
		Response(connection.responseCode, readInputStream(connection), null)
	} else {
		Response(connection.responseCode, "", null)
	}

}

fun getRaw(url: String, cookieString: String? = null, additionalHeaders: Map<String, String>? = null): RawResponse {

	val connection = URL(url).openConnection() as HttpURLConnection
	connection.setRequestProperty("Accept-Encoding", "gzip")

	additionalHeaders?.entries?.forEach {
		connection.setRequestProperty(it.key, it.value)
	}

	if (cookieString != null) {
		connection.setRequestProperty("Cookie", cookieString)
	}

	return if (connection.responseCode != 404 && connection.responseCode != 403) {
		RawResponse(connection.responseCode, readRawInputStream(connection), null)
	} else {
		RawResponse(connection.responseCode, ByteArray(0), null)
	}

}

fun post(url: String, data: List<Pair<String, String>>, cookieString: String? = null): Response {

	val dataBytes = if (data.isNotEmpty()) { data.joinToString("&", transform = { pair ->
		"${pair.first}=${URLEncoder.encode(pair.second, "UTF-8")}"
	}).toByteArray() } else { null }

	val connection = URL(url).openConnection() as HttpURLConnection
	connection.doOutput = true
	connection.requestMethod = "POST"
	connection.setRequestProperty("Accept-Encoding", "gzip")

	if (dataBytes != null) {
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
		connection.setRequestProperty("Content-Length", dataBytes.size.toString())
	}

	if (cookieString != null) {
		connection.setRequestProperty("Cookie", cookieString)
	}

	if (data.isNotEmpty() && dataBytes != null) {
		connection.outputStream.write(dataBytes)
	}

	val cookie = if (connection.headerFields["Set-Cookie"] != null) connection.headerFields["Set-Cookie"]!!.first() else null

	return Response(connection.responseCode, readInputStream(connection), cookie)

}

private fun readInputStream(connection: HttpURLConnection): String {

	val reader = if (connection.contentEncoding == "gzip") {
		log.info("Reading data (compressed) from '${connection.url}'")
		InputStreamReader(GZIPInputStream(connection.inputStream))
	} else {
		log.info("Reading data (uncompressed) from '${connection.url}'")
		InputStreamReader(connection.inputStream)
	}

	val output = StringBuilder()

	while (true) {

		val char = reader.read()

		if (char != -1) {
			output.append(char.toChar())
		} else {
			break
		}

	}

	return output.toString()

}

private fun readRawInputStream(connection: HttpURLConnection): ByteArray {

	val reader = if (connection.contentEncoding == "gzip") {
		log.info("Reading data (compressed) from '${connection.url}'")
		GZIPInputStream(connection.inputStream)
	} else {
		log.info("Reading data (uncompressed) from '${connection.url}'")
		connection.inputStream
	}

	val output = ByteArrayOutputStream(connection.contentLength)

	while (true) {

		val char = reader.read()

		if (char != -1) {
			output.write(char)
		} else {
			break
		}

	}

	return output.toByteArray()

}
