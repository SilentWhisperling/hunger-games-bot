package net.krazyweb.util

import com.mortennobel.imagescaling.ResampleFilters
import com.mortennobel.imagescaling.ResampleOp
import net.krazyweb.hungergames.data.District
import net.krazyweb.hungergames.data.Tribute
import net.krazyweb.hungergames.game.StatisticsKey
import net.krazyweb.hungergames.game.TributeStatisticsSubscriber
import java.awt.Color
import java.awt.Font
import java.awt.Graphics2D
import java.awt.RenderingHints
import java.awt.color.ColorSpace
import java.awt.font.TextAttribute
import java.awt.font.TextLayout
import java.awt.image.BufferedImage
import java.awt.image.ColorConvertOp
import javax.imageio.ImageIO
import kotlin.math.ceil
import kotlin.math.max
import kotlin.math.min

//TODO General cleanup and refactoring; maybe some layout tools as well?
class ImageUtils {

	companion object {

		private const val iconSize = 120
		private const val iconBorderSize = 1
		private const val portraitSpacing = 20
		private const val portraitPadding = 8
		private const val districtNameHeight = 20
		private const val districtNamePadding = 20
		private const val tributeDataHeight = 110

		private const val columnSpacing = 40
		private const val rowSpacing = 10
		private const val eventParticipantsSpacing = 25

		private fun resizeImage(image: BufferedImage, size: Int): BufferedImage {
			return resizeImage(image, size, size)
		}

		private fun resizeImage(image: BufferedImage, width: Int, height: Int): BufferedImage {

			val resizeOp = ResampleOp(width, height)
			resizeOp.filter = ResampleFilters.getLanczos3Filter()

			return resizeOp.filter(image, null)

		}

		private fun desaturate(image: BufferedImage): BufferedImage {
			return ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null).filter(image, null)
		}

		fun createScoreboard(districts: List<District>, tributeStatistics: TributeStatisticsSubscriber): BufferedImage {

			val columns = min(4, districts.size)
			val rows = ceil(districts.size / columns.toDouble()).toInt()

			val districtWidth = (iconSize + iconBorderSize * 2) * 2 + portraitSpacing
			val districtHeight = (iconSize + iconBorderSize * 2) + districtNameHeight + tributeDataHeight + portraitPadding * 2

			val totalHeight = rowSpacing * 2 + rowSpacing * (rows - 1) + rows * districtHeight
			val totalWidth = districtWidth * columns + columnSpacing * (columns + 1)

			val image = BufferedImage(totalWidth, totalHeight, BufferedImage.TYPE_INT_ARGB)
			val g = getImageGraphics(image)
			g.font = getFont().deriveFont(14.0f)
			g.font = g.font.deriveFont(Font.BOLD)

			//Background color
			g.color = Color.decode("#5d5050")
			g.fillRect(0, 0, totalWidth, totalHeight)

			//TODO Try to genericize this a bit so more than two tributes can be in a district and it won't break anything
			districts.forEachIndexed { index, district ->

				val districtX = index % columns
				val districtY = index / columns

				val startX = districtX * districtWidth + districtX * columnSpacing + columnSpacing
				val startY = districtY * districtHeight + districtY * rowSpacing + rowSpacing * 2

				val imageOneX = startX
				val imageOneY = startY + districtNameHeight + districtNamePadding

				val imageTwoX = startX + portraitSpacing + iconSize
				val imageTwoY = startY + districtNameHeight + districtNamePadding

				g.color = Color.decode("#000000")
				g.fillRect(imageOneX - iconBorderSize, imageOneY - iconBorderSize, iconSize + iconBorderSize * 2, iconSize + iconBorderSize * 2)
				g.fillRect(imageTwoX - iconBorderSize, imageTwoY - iconBorderSize, iconSize + iconBorderSize * 2, iconSize + iconBorderSize * 2)

				val resizedFirst = if (district.first.alive) {
					resizeImage(district.first.image, iconSize)
				} else {
					resizeImage(desaturate(district.first.image), iconSize)
				}

				val resizedSecond = if (district.second.alive) {
					resizeImage(district.second.image, iconSize)
				} else {
					resizeImage(desaturate(district.second.image), iconSize)
				}

				g.drawImage(resizedFirst, imageOneX, imageOneY, iconSize, iconSize, null)
				g.drawImage(resizedSecond, imageTwoX, imageTwoY, iconSize, iconSize, null)

				val oldFont = g.font
				g.font = g.font.deriveFont(districtNameHeight.toFloat())

				val districtTextBounds = getPreciseTextBounds("District ${district.number}", g)
				val districtNameX = districtTextBounds.offsetX + startX + (districtWidth - districtTextBounds.width) / 2
				val districtNameY = (districtTextBounds.offsetY + startY).toInt()

				g.color = Color.decode("#ffffff")
				g.drawString("District ${district.number}", districtNameX, districtNameY)

				g.font = oldFont

				var textHeightSoFar = 0.0f

				var tributeNameTextBounds = getPreciseTextBounds(district.first.name, g)
				var tributeNameX = tributeNameTextBounds.offsetX + startX + iconBorderSize
				var tributeNameY = tributeNameTextBounds.offsetY.toInt() + imageOneY + iconSize + iconBorderSize * 2 + portraitPadding

				textHeightSoFar += drawWrappedString(district.first.name, iconSize, tributeNameX, tributeNameY, g)
				if (district.first.alive) {
					g.color = Color.decode("#66ff66")
					textHeightSoFar += drawWrappedString("Alive", iconSize, tributeNameX, (tributeNameY + textHeightSoFar).toInt(), g)
				} else {
					g.color = Color.decode("#ff6666")
					textHeightSoFar += drawWrappedString("Deceased", iconSize, tributeNameX, (tributeNameY + textHeightSoFar).toInt(), g)
				}

				g.color = Color.decode("#ffffff")

				val firstTributeKills = tributeStatistics.getStatistics(district.first.discordId).get(StatisticsKey.KILLS, 0) as Int
				if (firstTributeKills == 1) {
					textHeightSoFar += drawWrappedString("1 kill", iconSize, tributeNameX, (tributeNameY + textHeightSoFar).toInt(), g)
				} else if (firstTributeKills > 1) {
					textHeightSoFar += drawWrappedString("$firstTributeKills kills", iconSize, tributeNameX, (tributeNameY + textHeightSoFar).toInt(), g)
				}

				textHeightSoFar = 0.0f
				tributeNameTextBounds = getPreciseTextBounds(district.second.name, g)
				tributeNameX = tributeNameTextBounds.offsetX + startX + iconBorderSize + iconSize + portraitSpacing
				tributeNameY = tributeNameTextBounds.offsetY.toInt() + imageTwoY + iconSize + iconBorderSize * 2 + portraitPadding

				textHeightSoFar += drawWrappedString(district.second.name, iconSize, tributeNameX, tributeNameY, g)
				if (district.second.alive) {
					g.color = Color.decode("#66ff66")
					textHeightSoFar += drawWrappedString("Alive", iconSize, tributeNameX, (tributeNameY + textHeightSoFar).toInt(), g)
				} else {
					g.color = Color.decode("#ff6666")
					textHeightSoFar += drawWrappedString("Deceased", iconSize, tributeNameX, (tributeNameY + textHeightSoFar).toInt(), g)
				}

				g.color = Color.decode("#ffffff")

				val secondTributeKills = tributeStatistics.getStatistics(district.second.discordId).get(StatisticsKey.KILLS, 0) as Int
				if (secondTributeKills == 1) {
					textHeightSoFar += drawWrappedString("1 kill", iconSize, tributeNameX, (tributeNameY + textHeightSoFar).toInt(), g)
				} else if (secondTributeKills > 1) {
					textHeightSoFar += drawWrappedString("$secondTributeKills kills", iconSize, tributeNameX, (tributeNameY + textHeightSoFar).toInt(), g)
				}

			}

			return image

		}

		fun createEventParticipantsImage(tributes: List<Tribute>): BufferedImage  {

			val width = (iconSize + iconBorderSize * 2) * tributes.size + (tributes.size - 1) * eventParticipantsSpacing + eventParticipantsSpacing * 2
			val height = (iconSize + iconBorderSize * 2) + eventParticipantsSpacing * 2

			val image = BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)
			val g = getImageGraphics(image)

			//Background color
			g.color = Color.decode("#5d5050")
			g.fillRect(0, 0, width, height)

			tributes.forEachIndexed { index, tribute ->

				val xOffset = index * (iconSize + iconBorderSize * 2) + index * eventParticipantsSpacing + eventParticipantsSpacing
				val yOffset = eventParticipantsSpacing

				g.color = Color.decode("#000000")
				g.fillRect(xOffset - iconBorderSize, yOffset - iconBorderSize, iconSize + iconBorderSize * 2, iconSize + iconBorderSize * 2)

				val resizedImage = resizeImage(tribute.image, iconSize)
				g.drawImage(resizedImage, xOffset, yOffset, iconSize, iconSize, null)

			}

			return image

		}

		fun createArenaEventImage(eventName: String): BufferedImage {

			val surroundingPadding = 40
			val textSpacing = 10

			val tempImage = BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB)
			var g = getImageGraphics(tempImage)
			g.font = getFont().deriveFont(24.0f)
			g.font = g.font.deriveFont(Font.BOLD)

			val arenaEventTextBounds = getPreciseTextBounds("Arena Event", g)

			g.font = getFont().deriveFont(36.0f)

			val eventNameTextBounds = getPreciseTextBounds(eventName, g)

			val width = max(200, surroundingPadding * 2 + max(arenaEventTextBounds.width, eventNameTextBounds.width))
			val height = (arenaEventTextBounds.offsetY + eventNameTextBounds.offsetY + surroundingPadding * 2 + textSpacing).toInt()

			val image = BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)
			g = getImageGraphics(image)
			g.font = getFont().deriveFont(24.0f)
			g.font = g.font.deriveFont(Font.BOLD)

			//Background color
			g.color = Color.decode("#5d5050")
			g.fillRect(0, 0, width, height)

			g.color = Color.decode("#ffffff")
			g.drawString("Arena Event", (width - arenaEventTextBounds.width) / 2, surroundingPadding + arenaEventTextBounds.offsetY.toInt() - 5)

			g.color = Color.decode("#efd600")
			g.font = getFont().deriveFont(36.0f)
			g.drawString(eventName, (width - eventNameTextBounds.width) / 2, surroundingPadding + textSpacing + arenaEventTextBounds.offsetY.toInt() + eventNameTextBounds.offsetY.toInt())

			return image

		}

		fun createFallenTributesImage(tributes: List<Tribute>, districts: List<District>): BufferedImage {

			val tempImage = BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB)
			var g = getImageGraphics(tempImage)
			g.font = getFont().deriveFont(24.0f)
			g.font = g.font.deriveFont(Font.BOLD)

			val titleTextBounds = getPreciseTextBounds("Fallen Tributes", g)

			g.font = g.font.deriveFont(14.0f)
			val tributeTextBounds = getPreciseTextBounds("Fallen Tributes", g)

			val tributeSpacing = 40
			val textPadding = 30

			val width = max(titleTextBounds.width + tributeSpacing * 2, (iconSize + iconBorderSize * 2) * tributes.size + (tributes.size - 1) * tributeSpacing + tributeSpacing * 2)
			val height = (iconSize + iconBorderSize * 2) + tributeSpacing * 2 + tributeTextBounds.offsetY.toInt() * 2 + titleTextBounds.offsetY.toInt() + textPadding + textPadding

			val image = BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)
			g = getImageGraphics(image)
			g.font = getFont().deriveFont(24.0f)

			//Background color
			g.color = Color.decode("#5d5050")
			g.fillRect(0, 0, width, height)

			g.color = Color.decode("#ffffff")
			g.drawString("Fallen Tributes", (width - titleTextBounds.width) / 2, textPadding + titleTextBounds.offsetY.toInt())
			g.font = g.font.deriveFont(14.0f)


			tributes.forEachIndexed { index, tribute ->

				val district = districts.single { it.first.discordId == tribute.discordId || it.second.discordId == tribute.discordId }

				val xOffset = if (tributes.size == 1) {
					(width - (iconSize + iconBorderSize * 2)) / 2
				} else {
					index * (iconSize + iconBorderSize * 2) + index * tributeSpacing + tributeSpacing
				}
				val yOffset = tributeSpacing + titleTextBounds.offsetY.toInt() + textPadding

				g.color = Color.decode("#000000")
				g.fillRect(xOffset - iconBorderSize, yOffset - iconBorderSize, iconSize + iconBorderSize * 2, iconSize + iconBorderSize * 2)

				val resizedImage = desaturate(resizeImage(tribute.image, iconSize))
				g.drawImage(resizedImage, xOffset, yOffset, iconSize, iconSize, null)

				var textOffsetY = (yOffset + iconSize + iconBorderSize * 2 + textPadding).toFloat()
				g.color = Color.decode("#ff9c00")
				textOffsetY += drawWrappedString(tribute.name, iconSize, xOffset, textOffsetY.toInt(), g)
				g.color = Color.decode("#ffffff")
				drawWrappedString("District ${district.number}", iconSize, xOffset, textOffsetY.toInt(), g)

			}

			return image

		}

		fun createTimeChangeImage(timeOfDay: String, dayNumber: Int): BufferedImage { //Day/Night

			val sunMoonIconSize = 100
			val timeChangePadding = 30

			val time = if (timeOfDay.toLowerCase() == "day") {
				"Day"
			} else {
				"Night"
			}

			val text = "$time $dayNumber"

			val tempImage = BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB)
			var g = getImageGraphics(tempImage)
			g.font = getFont().deriveFont(24.0f)
			g.font = g.font.deriveFont(Font.BOLD)

			val textBounds = getPreciseTextBounds(text, g)

			val width = (timeChangePadding * 3 + sunMoonIconSize + textBounds.width)
			val height = sunMoonIconSize + timeChangePadding * 2

			val image = BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)
			g = getImageGraphics(image)
			g.font = getFont().deriveFont(24.0f)
			g.font = g.font.deriveFont(Font.BOLD)

			//Background color
			g.color = Color.decode("#5d5050")
			g.fillRect(0, 0, width, height)

			val icon = resizeImage(Thread.currentThread().contextClassLoader.getResourceAsStream("icons/feather/${if (time == "Day") "sun" else "moon" }.png").use {
				ImageIO.read(it)
			}, sunMoonIconSize)

			g.drawImage(icon, timeChangePadding, timeChangePadding, null)

			val textX = timeChangePadding * 2 + sunMoonIconSize
			val textY = ((height + textBounds.offsetY) / 2 - 4).toInt()

			g.color = Color.decode("#ffffff")
			g.drawString(text, textX, textY)

			return image

		}

		fun createWinnerImage(winner: Tribute, districts: List<District>): BufferedImage {

			val district = districts.single { it.first == winner || it.second == winner }

			val winnerTitle = "The Winner"
			val winnerDescription = "The winner is ${winner.name} from District ${district.number}!"

			val winnerIconSize = 200
			val padding = 20

			val tempImage = BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB)
			var g = getImageGraphics(tempImage)
			g.font = getFont().deriveFont(24.0f)
			g.font = g.font.deriveFont(Font.BOLD)

			val winnerTitleTextBounds = getPreciseTextBounds(winnerTitle, g)
			val winnerNameTextBounds = getPreciseTextBounds(winnerDescription, g)

			val width = max((winnerIconSize * 1.5).toInt(), max(winnerNameTextBounds.width, winnerIconSize) + eventParticipantsSpacing * 2)
			val height = (winnerTitleTextBounds.offsetY + winnerNameTextBounds.offsetY + padding * 2 + winnerIconSize + eventParticipantsSpacing * 2).toInt()

			val image = BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)
			g = getImageGraphics(image)
			g.font = getFont().deriveFont(24.0f)
			g.font = g.font.deriveFont(Font.BOLD)

			//Background color
			g.color = Color.decode("#5d5050")
			g.fillRect(0, 0, width, height)

			val resized = resizeImage(winner.image, winnerIconSize)

			val imageX = (width - winnerIconSize) / 2
			val imageY = (eventParticipantsSpacing + padding + winnerTitleTextBounds.offsetY).toInt()

			g.color = Color.decode("#000000")
			g.fillRect(imageX - iconBorderSize, imageY - iconBorderSize, winnerIconSize + iconBorderSize * 2, winnerIconSize + iconBorderSize * 2)

			g.drawImage(resized, imageX, imageY, winnerIconSize, winnerIconSize, null)

			g.color = Color.decode("#ffffff")
			g.drawString(winnerTitle, (width - winnerTitleTextBounds.width) / 2, padding + winnerTitleTextBounds.offsetY.toInt() - winnerTitleTextBounds.leading)

			var descriptionX = (width - winnerNameTextBounds.width) / 2
			g.drawString("The winner is", descriptionX, imageY + winnerIconSize + padding + winnerNameTextBounds.offsetY.toInt() - winnerTitleTextBounds.leading)
			descriptionX += getPreciseTextBounds("The winner i s", g).width
			g.color = Color.decode("#ff9c00")
			g.drawString(winner.name, descriptionX, imageY + winnerIconSize + padding + winnerNameTextBounds.offsetY.toInt() - winnerTitleTextBounds.leading)
			descriptionX += getPreciseTextBounds(winner.name + " ", g).width
			g.color = Color.decode("#ffffff")
			g.drawString(" from District ${district.number}!", descriptionX, imageY + winnerIconSize + padding + winnerNameTextBounds.offsetY.toInt() - winnerTitleTextBounds.leading)

			return image

		}

		private fun getFont(): Font {
			Thread.currentThread().contextClassLoader.getResourceAsStream("fonts/open-sans/OpenSans-Bold.ttf").use {
				return Font.createFont(Font.TRUETYPE_FONT, it).deriveFont(mapOf<TextAttribute, Any>(TextAttribute.TRACKING to 0.01))
			}
		}

		private fun drawWrappedString(name: String, maxWidth: Int, x: Int, y: Int, g: Graphics2D): Float {

			val nameHasSpaces = name.contains(" ")
			val stringsToDraw = mutableListOf<String>()

			if (nameHasSpaces) {

				var currentLine = ""

				val nameParts = name.split(" ")
				nameParts.forEach {
					if (currentLine.isNotBlank() && getPreciseTextBounds("$currentLine $it", g).width >= maxWidth) {
						stringsToDraw += currentLine
						currentLine = it
					} else {
						if (currentLine.isNotBlank()) {
							currentLine += " "
						}
						currentLine += it
					}
				}

				if (currentLine.isNotBlank()) {
					stringsToDraw += currentLine
				}

			} else {

				var currentLine = ""

				val nameParts = name.toCharArray()
				nameParts.map { it.toString() }.forEach {
					if (currentLine.isNotBlank() && getPreciseTextBounds(currentLine + it, g).width >= maxWidth) {
						stringsToDraw += currentLine
						currentLine = it
					} else {
						currentLine += it
					}
				}

				if (currentLine.isNotBlank()) {
					stringsToDraw += currentLine
				}

			}

			var verticalOffset = 0.0f

			stringsToDraw.forEach {

				val textBounds = getPreciseTextBounds(it, g)
				val xAlign = (maxWidth - textBounds.width) / 2

				g.drawString(it, x + xAlign, y + verticalOffset.toInt())

				verticalOffset += textBounds.leading + textBounds.offsetY

			}

			return verticalOffset

		}

		private fun getImageGraphics(image: BufferedImage): Graphics2D {
			val g = image.createGraphics()
			g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC)
			g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON)
			g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP)
			return g
		}

		private data class FontBounds(val width: Int, val offsetX: Int, val offsetY: Float, val leading: Int)

		private fun getPreciseTextBounds(text: String, g: Graphics2D): FontBounds {

			val fontMetrics = TextLayout(text, g.font, g.fontRenderContext)

			val width = fontMetrics.visibleAdvance
			val offsetX = fontMetrics.advance - fontMetrics.visibleAdvance - 1
			val offsetY = fontMetrics.ascent - fontMetrics.baseline + 1

			return FontBounds(width.toInt(), offsetX.toInt(), offsetY, 3)

		}

	}

}
