package net.krazyweb.util

import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardOpenOption
import kotlin.math.min

class FlatFileMapDB(private val path: Path) {

	fun initialize() {
		Files.createDirectories(path.parent)
		if (Files.notExists(path)) {
			Files.write(path, "".toByteArray(), StandardOpenOption.CREATE_NEW)
		}
	}

	fun insertOrUpdate(key: String, vararg values: String) {

		val lines = Files.readAllLines(path)

		lines.removeIf { it.startsWith(key) }

		lines += if (values.isNotEmpty()) {
			"$key\t${values.joinToString("\t")}"
		} else {
			key
		}

		Files.write(path, lines.joinToString("\n").toByteArray(Charset.forName("UTF-8")), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE)

	}

	fun insertOrUpdate(key: String, values: List<String>) {
		insertOrUpdate(key, *values.toTypedArray())
	}

	fun get(key: String): List<String> {
		val line = Files.readAllLines(path).single { it.startsWith(key) }
		return line.substring(min(key.length + 1, line.length)).split("\t")
	}

	fun getAllLines(): Map<String, List<String>> {

		val output: MutableMap<String, List<String>> = mutableMapOf()

		Files.readAllLines(path).forEach {
			val key = it.split("\t")[0]
			output[key] = get(key)
		}

		return output

	}

	fun getAllKeys(): List<String> {
		return getAllLines().keys.toList()
	}

	fun delete(key: String) {

		val lines = Files.readAllLines(path)

		lines.removeIf { it.startsWith(key) }

		Files.write(path, lines.joinToString("\n").toByteArray(Charset.forName("UTF-8")), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE)

	}

	fun delete(keys: List<String>) {

		val lines = Files.readAllLines(path)

		lines.removeIf { it.split("\t")[0] in keys }

		Files.write(path, lines.joinToString("\n").toByteArray(Charset.forName("UTF-8")), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE)

	}


}
