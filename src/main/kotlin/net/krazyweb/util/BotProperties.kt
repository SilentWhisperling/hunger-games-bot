package net.krazyweb.util

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.io.BufferedReader
import java.io.FileInputStream
import java.io.InputStreamReader
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*

class BotProperties {

	private val log: Logger = LogManager.getLogger(BotProperties::class.java)

	private val javaProperties: Properties by lazy {
		val propertiesLocation = System.getenv("PROPERTIES_LOCATION")
		Properties().apply {
			log.info("Loading properties from $propertiesLocation")
			load(BufferedReader(InputStreamReader(FileInputStream(propertiesLocation))))
			log.info("Properties loaded")
		}
	}

	fun getStringProperty(key: String): String {
		return javaProperties[key].toString()
	}

	fun getPathProperty(key: String): Path {
		return Paths.get(javaProperties[key].toString())
	}

	fun getIntProperty(key: String): Int {
		return javaProperties[key].toString().toInt()
	}

	fun getLongProperty(key: String): Long {
		return javaProperties[key].toString().toLong()
	}

	fun getDoubleProperty(key: String): Double {
		return javaProperties[key].toString().toDouble()
	}

	fun getBooleanProperty(key: String): Boolean {
		return javaProperties[key].toString().toBoolean()
	}

}

