package net.krazyweb.hungergames.services

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import net.krazyweb.hungergames.data.PlayerPreferenceKey
import net.krazyweb.hungergames.data.PlayerPreferences
import net.krazyweb.util.BotProperties
import java.nio.file.Files


class PlayerPreferenceService(private val properties: BotProperties) {

	companion object {
		val lock = Object()
	}

	private val defaults = mutableMapOf(
			PlayerPreferenceKey.PRONOUNS to "they"
	)

	fun get(user: Long, playerPreferenceKey: PlayerPreferenceKey): String {

		val preferences = readPreferences()

		if (!hasUser(user, preferences)) {
			preferences += PlayerPreferences(user)
		}

		return getForUser(user, preferences).preferences[playerPreferenceKey] ?: defaults[playerPreferenceKey]!!

	}

	fun getAll(playerPreferenceKey: PlayerPreferenceKey): Map<Long, String> {

		val preferences = readPreferences()

		return preferences.filter { it.preferences.containsKey(playerPreferenceKey) }.map { it.user to it.preferences[playerPreferenceKey]!! }.toMap()

	}

	fun save(user: Long, playerPreferenceKey: PlayerPreferenceKey, value: String) {

		val preferences = readPreferences()

		if (!hasUser(user, preferences)) {
			preferences += PlayerPreferences(user)
		}

		val preference = getForUser(user, preferences)
		preference.preferences[playerPreferenceKey] = value

		savePreferences(preferences)

	}

	fun delete(user: Long, playerPreferenceKey: PlayerPreferenceKey) {

		val preferences = readPreferences()

		if (hasUser(user, preferences)) {
			val preference = getForUser(user, preferences)
			preference.preferences.remove(playerPreferenceKey)
			savePreferences(preferences)
		}

	}

	fun hasPreferenceForUser(user: Long, playerPreferenceKey: PlayerPreferenceKey): Boolean {

		val preferences = readPreferences()

		if (!hasUser(user, preferences)) {
			return false
		}

		val userPreferences = getForUser(user, preferences)

		return userPreferences.preferences.containsKey(playerPreferenceKey)

	}

	private fun hasUser(user: Long, preferences: MutableList<PlayerPreferences>): Boolean {
		return preferences.any { it.user == user }
	}

	private fun getForUser(user: Long, preferences: MutableList<PlayerPreferences>): PlayerPreferences {
		return preferences.single { it.user == user }
	}

	private fun readPreferences(): MutableList<PlayerPreferences> {

		val file = properties.getPathProperty("workingDirectory").resolve("preferences.json")

		if (!Files.exists(file) || Files.size(file) == 0L) {
			savePreferences(mutableListOf())
		}

		synchronized(lock) {
			return jacksonObjectMapper().readValue(file.toFile(), object : TypeReference<MutableList<PlayerPreferences>>(){})
		}

	}

	private fun savePreferences(preferences: MutableList<PlayerPreferences>) {
		val file = properties.getPathProperty("workingDirectory").resolve("preferences.json")
		synchronized(lock) {
			Files.write(file, jacksonObjectMapper().writeValueAsBytes(preferences))
		}
	}

}


