package net.krazyweb.hungergames.services

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import net.krazyweb.hungergames.data.Pronouns
import net.krazyweb.util.BotProperties

class PronounService(val properties: BotProperties) {

	fun getPronounList(): List<Pronouns> {
		val file = properties.getPathProperty("workingDirectory").resolve("pronouns.json")
		return jacksonObjectMapper().readValue(file.toFile(), object : TypeReference<List<Pronouns>>() {})
	}

	fun getPronouns(nominativePronoun: String): Pronouns {

		val file = properties.getPathProperty("workingDirectory").resolve("pronouns.json")

		val pronounList = jacksonObjectMapper().readValue(file.toFile(), object : TypeReference<List<Pronouns>>() {}) as List<Pronouns>

		return pronounList.singleOrNull { it.nominative == nominativePronoun } ?: throw IllegalArgumentException(
				"No set of pronouns could be found for the nominative pronoun '$nominativePronoun'. Valid options are ${pronounList.joinToString("/"){it.nominative}}")

	}

}
