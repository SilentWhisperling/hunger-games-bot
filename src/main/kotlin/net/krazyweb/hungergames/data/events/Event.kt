package net.krazyweb.hungergames.data.events

import net.krazyweb.hungergames.data.DeathType
import net.krazyweb.hungergames.data.Item
import net.krazyweb.hungergames.data.Tribute
import net.krazyweb.hungergames.game.ArenaData
import kotlin.random.Random

//TODO Random quantities (1 to 4 explosives, for example)
data class ItemReference(val id: Int, val owner: Int? = null, val quantity: Int = 1, val tags: List<String>)
data class InventoryChange(val from: Int?, val to: Int?, val item: Int)

data class Event(val constructors: List<EventConstructor>,
                 val timeOfDay: String = "any",
                 val event: String? = null,
                 val killers: List<Int> = listOf(),
                 val killed: List<Int> = listOf(),
                 val deathType: DeathType? = null,
                 val itemReferences: List<ItemReference> = listOf(),
                 val inventoryChanges: List<InventoryChange> = listOf()) {

	fun runEvent(tributes: List<Tribute>, allItems: List<Item> = mutableListOf(), random: Random? = null): String {

		getKilledTributes(tributes).forEach { it.alive = false }

		val items: MutableList<Item> = mutableListOf()

		itemReferences.forEach { itemReference ->
			items += if (itemReference.owner != null) {
				tributes[itemReference.owner].inventory.filter { it.value >= itemReference.quantity }.map { it.key }.filter { item ->
					item !in items && item.tags.containsAll(itemReference.tags) || itemReference.tags.contains("any")
				}.random(random!!)
			} else {
				allItems.filter { item -> item !in items && item.tags.containsAll(itemReference.tags) || itemReference.tags.contains("any") }.random(random!!)
			}
		}

		inventoryChanges.forEach {
			if (it.from != null) {
				tributes[it.from].removeItem(items[it.item], itemReferences[it.item].quantity)
			}
			if (it.to != null) {
				tributes[it.to].addItem(items[it.item].clone(), itemReferences[it.item].quantity)
			}
		}

		return constructors.joinToString("") { it.generate(this, tributes, itemReferences, items) }

	}

	fun getKillerTributes(tributes: List<Tribute>): List<Tribute> {
		return killers.map { tributes[it] }
	}

	fun getKilledTributes(tributes: List<Tribute>): List<Tribute> {
		return killed.map { tributes[it] }
	}

	fun getTotalNumberOfTributes(): Int {
		return constructors.filterIsInstance<PlayerNameEventConstructor>().distinctBy { it.id }.count()
	}

	fun canRunEvent(tributes: List<Tribute>, fatal: Boolean, itemRequired: Boolean, arenaData: ArenaData): Boolean {

		if (fatal && killed.isEmpty() || !fatal && killed.isNotEmpty()) {
			return false
		}

		if (constructors.filterIsInstance<PlayerNameEventConstructor>().distinctBy { it.id }.count() != tributes.size) {
			return false
		}

		if (itemRequired && !constructors.filterIsInstance<ItemNameEventConstructor>().any()) {
			return false
		}

		if (timeOfDay != "any" && timeOfDay.toLowerCase() != arenaData.time.toLowerCase()) {
			return false
		}

		if (arenaData.activeEvent != event) {
			return false
		}

		val selectedItems = mutableListOf<Item>()

		itemReferences.forEach { itemReference ->
			if (itemReference.owner != null) {
				val validItems = tributes[itemReference.owner].inventory.filter { it.value >= itemReference.quantity }.map { it.key }.filter { item -> item !in selectedItems && item.tags.containsAll(itemReference.tags) || itemReference.tags.contains("any") }
				if (validItems.isEmpty()) {
					return false
				} else {
					selectedItems += validItems.first()
				}
			}
		}

		return true

	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as Event

		if (constructors != other.constructors) return false
		if (timeOfDay != other.timeOfDay) return false
		if (event != other.event) return false
		if (killers != other.killers) return false
		if (killed != other.killed) return false
		if (deathType != other.deathType) return false
		if (itemReferences != other.itemReferences) return false
		if (inventoryChanges != other.inventoryChanges) return false

		return true
	}

	override fun hashCode(): Int {
		var result = constructors.hashCode()
		result = 31 * result + timeOfDay.hashCode()
		result = 31 * result + (event?.hashCode() ?: 0)
		result = 31 * result + killers.hashCode()
		result = 31 * result + killed.hashCode()
		result = 31 * result + (deathType?.hashCode() ?: 0)
		result = 31 * result + itemReferences.hashCode()
		result = 31 * result + inventoryChanges.hashCode()
		return result
	}


}
