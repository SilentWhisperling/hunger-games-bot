package net.krazyweb.hungergames.data

enum class DeathType {
	MURDER, ACCIDENTAL, INTENTIONAL, ENVIRONMENTAL
}
