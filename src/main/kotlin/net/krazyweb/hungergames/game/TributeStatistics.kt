package net.krazyweb.hungergames.game

enum class StatisticsKey {
	KILLS
}

data class TributeStatistics(val tributeId: Long, private val statistics: MutableMap<StatisticsKey, Any> = mutableMapOf()) {

	fun get(key: StatisticsKey, default: Any? = null): Any? {
		return if (statistics.containsKey(key)) {
			statistics[key]
		} else {
			default
		}
	}

	fun put(key: StatisticsKey, value: Any?) {
		if (value == null) {
			statistics.remove(key)
		} else {
			statistics[key] = value
		}
	}

}
