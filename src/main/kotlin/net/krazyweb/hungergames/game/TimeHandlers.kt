package net.krazyweb.hungergames.game

import kotlin.random.Random

interface TimeHandler {
	fun getNextTimeHandler(random: Random, hungerGame: HungerGame): TimeHandler
	fun getName(): String
	fun arenaEvent(): String? {
		return null
	}
	fun shouldIncrementDay(): Boolean {
		return false
	}
	fun playersCanTakeActions(): Boolean {
		return true
	}
}

class BloodbathTimeHandler : TimeHandler {

	override fun getNextTimeHandler(random: Random, hungerGame: HungerGame): TimeHandler {
		return DayTimeHandler()
	}

	override fun arenaEvent(): String {
		return "The Bloodbath"
	}

	override fun getName(): String {
		return "The Bloodbath"
	}

}

class DayTimeHandler : TimeHandler {

	override fun getNextTimeHandler(random: Random, hungerGame: HungerGame): TimeHandler {
		return if (hungerGame.day == 6) {
			ArenaEventTimeHandler("The Feast")
		} else if (random.nextInt(8) == 0 && hungerGame.day > 1) {
			val possibleArenaEvents = hungerGame.getAllArenaEvents()
			if (possibleArenaEvents.isEmpty()) {
				FallenTributesTimeHandler()
			} else {
				ArenaEventTimeHandler(possibleArenaEvents.random(random))
			}
		} else {
			FallenTributesTimeHandler()
		}
	}

	override fun getName(): String {
		return "Day"
	}

}

class NightTimeHandler : TimeHandler {

	override fun getNextTimeHandler(random: Random, hungerGame: HungerGame): TimeHandler {
		return DayTimeHandler()
	}

	override fun getName(): String {
		return "Night"
	}

	override fun shouldIncrementDay(): Boolean {
		return true
	}

}

class FallenTributesTimeHandler : TimeHandler {

	override fun getNextTimeHandler(random: Random, hungerGame: HungerGame): TimeHandler {
		return NightTimeHandler()
	}

	override fun getName(): String {
		return "Fallen Tributes"
	}

	override fun playersCanTakeActions(): Boolean {
		return false
	}

}

class ArenaEventTimeHandler(val event: String) : TimeHandler {

	override fun getNextTimeHandler(random: Random, hungerGame: HungerGame): TimeHandler {
		return FallenTributesTimeHandler()
	}

	override fun arenaEvent(): String {
		return event
	}

	override fun getName(): String {
		return event
	}

}
