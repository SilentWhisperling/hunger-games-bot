package net.krazyweb.hungerbot

import net.krazyweb.hungerbot.commands.Command
import net.krazyweb.hungerbot.commands.botcontrol.AboutCommand
import net.krazyweb.hungerbot.commands.botcontrol.DisconnectCommand
import net.krazyweb.hungerbot.commands.botcontrol.HelpCommand
import net.krazyweb.hungerbot.commands.botcontrol.InviteCommand
import net.krazyweb.hungerbot.commands.game.*
import net.krazyweb.hungerbot.services.AutoGameService
import net.krazyweb.hungergames.game.BotGameEventSubscriber
import net.krazyweb.hungergames.game.HungerGame
import net.krazyweb.hungergames.game.TributeStatisticsSubscriber
import net.krazyweb.hungergames.services.*
import net.krazyweb.util.BotProperties
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.DiscordApi
import org.javacord.api.DiscordApiBuilder
import org.javacord.api.entity.intent.Intent
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.permission.PermissionType
import org.javacord.api.entity.permission.PermissionsBuilder
import java.nio.file.Files
import java.nio.file.Paths
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.system.exitProcess


var botStartTime = 0L

class HungerGamesBot {

	private val log: Logger = LogManager.getLogger(HungerGamesBot::class.java)

	val commands: MutableList<Command> = mutableListOf()

	var api: DiscordApi? = null

	val autoGameService = AutoGameService(this)

	var activeGame: HungerGame? = null //TODO Move this to a service, too? Seems weird to just have it in the main class. *Something* needs to be done with it. Probably.
	var tributeStatisticsSubscriber: TributeStatisticsSubscriber? = null
	var botGameEventSubscriber: BotGameEventSubscriber? = null

	fun start() {

		//TODO Abstraction layer for all this so making a bot is just defining the services/message handlers/pollers and properties location. Message forwarding/reconnecting/etc should all be handled by the layer.
		//TODO Split most of this out to a DiscordBot class to fulfill the above TODO?

		botStartTime = System.currentTimeMillis()

		log.info("Initializing Hunger Bot ${getVersion()}")

		val properties = BotProperties()

		api = DiscordApiBuilder()
				.setIntents(
						Intent.GUILDS,
						Intent.GUILD_MEMBERS,
						Intent.GUILD_BANS,
						Intent.GUILD_EMOJIS,
						Intent.GUILD_INTEGRATIONS,
						Intent.GUILD_WEBHOOKS,
						Intent.GUILD_VOICE_STATES,
						Intent.GUILD_MESSAGES,
						Intent.GUILD_MESSAGE_REACTIONS,
						Intent.DIRECT_MESSAGES,
						Intent.DIRECT_MESSAGE_REACTIONS
				)
				.setToken(properties.getStringProperty("token"))
				.login()
				.join()

		log.info(createInvite(api!!))

		log.info("Bot successfully connected!")

		if (api == null) {
			throw RuntimeException("API is null!")
		}

		val pronounService = PronounService(properties)
		val playerPreferenceService = PlayerPreferenceService(properties)

		autoGameService.initialize()

		commands += listOf(
				AboutCommand(),
				DisconnectCommand(this, properties),
				HelpCommand(this),
				InviteCommand(api!!, properties),
				StartGameCommand(api!!, this, properties, playerPreferenceService, pronounService).apply {
					api.addReactionAddListener(this)
					api.addReactionRemoveListener(this)
				},
				ShowDistrictsCommand(this, properties),
				SaveGameCommand(this, properties),
				LoadGameCommand(this, properties),
				GameNextCommand(this, properties),
				ReplayCommand(this, properties),
				ShowInventoryCommand(this),
				LeaderboardCommand(this, properties),
				TestEventJsonCommand(properties),
				AutoRunCommand(autoGameService, this, properties),
				PronounsPreferenceCommand(pronounService, playerPreferenceService),
				SubscribeCommand(playerPreferenceService),
				UnsubscribeCommand(playerPreferenceService),
				SetPreferenceCommand(properties, playerPreferenceService)
		)

		val prefixes = listOf(

				object : MessagePrefix {
					override fun matches(message: Message, messageContent: String): Boolean {
						return messageContent.startsWith(".")
					}
					override fun messageWithoutPrefix(messageContent: String): String {
						return messageContent.substring(1)
					}
				},

				object : MessagePrefix {
					override fun matches(message: Message, messageContent: String): Boolean {
						return message.mentionedUsers.any { it.isYourself }
					}
					override fun messageWithoutPrefix(messageContent: String): String {
						var output = messageContent
						val mention = "<@!${api!!.clientId}>"
						if (output.startsWith(mention)) {
							output = output.substring(mention.length).trim()
						}
						if (output.startsWith("!")) {
							output = output.substring(1).trim()
						}
						return output
					}
				}

		)

		api!!.addMessageCreateListener { event ->

			var messageContent = event.messageContent

			event.message.mentionedUsers.forEach {
				messageContent = messageContent.replace("<@${it.id}>", "")
			}

			messageContent = messageContent.trim()

			val possiblePrefixes = prefixes.filter { it.matches(event.message, messageContent) }

			if (possiblePrefixes.isNotEmpty() && !event.messageAuthor.isBotUser) {

				if (event.isServerMessage) {
					log.debug("Received message: ${event.server.get().name} -- #${event.serverTextChannel.get().name} ${event.messageAuthor.name} (${event.messageAuthor.id}): $messageContent")
				}

				if (event.isPrivateMessage) {
					log.debug("Received message: <PRIVATE> ${event.messageAuthor.name} (${event.messageAuthor.id}): $messageContent")
				}

				val messageCommand = possiblePrefixes.first().messageWithoutPrefix(messageContent.split(Regex("\\s+"))[0]).toLowerCase()
				var messageWithoutCommand = messageContent.substringAfter(" ")

				if (messageWithoutCommand == messageContent) {
					messageWithoutCommand = ""
				}

				commands.filter { messageCommand in it.aliases }.singleOrNull { it.permissions(event.message) }?.delegateMessage(event.message, messageWithoutCommand)

			}

		}

		api!!.addLostConnectionListener {
			log.debug("Lost connection to Discord.")
			if (activeGame != null && !activeGame!!.isFinished()) {

				val outputDirectory = properties.getPathProperty("workingDirectory").resolve("saved-games")
				Files.createDirectories(outputDirectory)

				val fileService = FileService(properties)
				val eventLoaderService = EventLoaderService(fileService, pronounService, playerPreferenceService)
				val itemLoaderService = ItemLoaderService(fileService)

				val dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss")
				val time = dateFormat.format(Instant.now())

				Files.write(outputDirectory.resolve("$time-disconnected.zip"), GamePersistenceService(fileService, eventLoaderService, itemLoaderService).save(activeGame!!))

			}
		}

		api!!.addResumeListener {
			log.debug("Resumed connection to Discord.")
		}

		api!!.addReconnectListener {
			log.debug("Reconnected to Discord.")
		}

	}

	fun stopBot() {

		log.info("Stopping services")
		autoGameService.stop()
		log.info("Stopped all services")

		log.info("Disconnecting from Discord")
		api?.disconnect()
		log.info("Disconnected from Discord")

		exitProcess(0)

	}

}

interface MessagePrefix {
	fun matches(message: Message, messageContent: String): Boolean
	fun messageWithoutPrefix(messageContent: String): String
}

fun getVersion(): String {

	return try {
		val properties = Properties().apply { load(HungerGamesBot::class.java.classLoader.getResourceAsStream("build.properties")) }
		"v" + properties.getProperty("version") + if (properties.getProperty("dirty").toBoolean()) { " (dirty)" } else { "" }
	} catch (e: Exception) {

		var version = HungerGamesBot::class.java.`package`.implementationVersion

		@Suppress("FoldInitializerAndIfToElvis")
		if (version == null) {
			version = Files.readAllLines(Paths.get("pom.xml")).first { it.trim().startsWith("<version>") }.trim().replace(Regex("</?version>"), "")
		}

		"v$version (local)"

	}

}

fun createInvite(api: DiscordApi): String {
	return api.createBotInvite(PermissionsBuilder().setAllDenied()
			.setAllowed(PermissionType.READ_MESSAGES)
			.setAllowed(PermissionType.SEND_MESSAGES)
			.setAllowed(PermissionType.EMBED_LINKS)
			.setAllowed(PermissionType.ATTACH_FILE)
			.setAllowed(PermissionType.READ_MESSAGE_HISTORY)
			.setAllowed(PermissionType.USE_EXTERNAL_EMOJIS)
			.setAllowed(PermissionType.ADD_REACTIONS)
			.setAllowed(PermissionType.MANAGE_MESSAGES)
			.build())
}

fun main() {
	HungerGamesBot().start()
}
