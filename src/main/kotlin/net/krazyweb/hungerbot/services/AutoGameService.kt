package net.krazyweb.hungerbot.services

import net.krazyweb.hungerbot.HungerGamesBot
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger

class AutoGameService(val bot: HungerGamesBot) {

	private var lastIssuedCommandTime = 0L

	private val shouldQuit = AtomicBoolean(false)

	val intervalInSeconds = AtomicInteger(20)
	val running = AtomicBoolean(false)

	val thread = Thread {

		while (!shouldQuit.get()) {

			Thread.sleep(1000)

			if (running.get()) {
				val time = System.currentTimeMillis()
				if (time - lastIssuedCommandTime > intervalInSeconds.get() * 1000) {
					lastIssuedCommandTime = time
					if (bot.activeGame != null) {
						bot.activeGame!!.next()
					} else {
						running.set(false)
					}
				}
			}

		}

	}

	fun initialize() {
		thread.start()
	}

	fun stop() {
		shouldQuit.set(true)
		thread.join(10000)
	}

}
