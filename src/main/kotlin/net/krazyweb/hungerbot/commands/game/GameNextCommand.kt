package net.krazyweb.hungerbot.commands.game

import net.krazyweb.hungerbot.HungerGamesBot
import net.krazyweb.hungerbot.Permissions
import net.krazyweb.hungerbot.commands.Command
import net.krazyweb.util.BotProperties
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder

class GameNextCommand(val bot: HungerGamesBot, val botProperties: BotProperties) : Command("next", description = "Progresses the active game by one step.", permissions = Permissions.role(botProperties.getLongProperty("gameHostRoleId"))) {

	val log: Logger = LogManager.getLogger(GameNextCommand::class.java)

	override fun handleMessage(message: Message, messageContent: String) {

		try {
			message.delete()
		} catch (e: Exception) {
			log.error("Error deleting .next message", e)
		}

		if (bot.activeGame != null) {
			bot.activeGame!!.next()
		} else {
			MessageBuilder().setContent("There is no game in progress. Please start a new game before trying to advance one.").send(message.channel)
		}

	}

}
