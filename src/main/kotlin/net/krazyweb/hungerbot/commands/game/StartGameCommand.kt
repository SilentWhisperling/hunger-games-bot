package net.krazyweb.hungerbot.commands.game

import net.krazyweb.hungerbot.HungerGamesBot
import net.krazyweb.hungerbot.Permissions
import net.krazyweb.hungerbot.commands.Command
import net.krazyweb.hungergames.data.PlayerPreferenceKey
import net.krazyweb.hungergames.data.Tribute
import net.krazyweb.hungergames.game.BotGameEventSubscriber
import net.krazyweb.hungergames.game.DeadlinessFactor
import net.krazyweb.hungergames.game.HungerGame
import net.krazyweb.hungergames.game.TributeStatisticsSubscriber
import net.krazyweb.hungergames.services.*
import net.krazyweb.util.BotProperties
import net.krazyweb.util.getRaw
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.DiscordApi
import org.javacord.api.entity.emoji.Emoji
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder
import org.javacord.api.entity.server.Server
import org.javacord.api.entity.user.User
import org.javacord.api.event.message.reaction.ReactionAddEvent
import org.javacord.api.event.message.reaction.ReactionRemoveEvent
import org.javacord.api.listener.message.reaction.ReactionAddListener
import org.javacord.api.listener.message.reaction.ReactionRemoveListener
import java.io.ByteArrayInputStream
import javax.imageio.ImageIO
import kotlin.concurrent.thread
import kotlin.math.abs
import kotlin.math.floor
import kotlin.math.min

class StartGameCommand(val api: DiscordApi, val bot: HungerGamesBot, val properties: BotProperties, val playerPreferenceService: PlayerPreferenceService, val pronounService: PronounService) : Command("start", arguments = "<number_of_tributes> <deadliness>` OR `<help>", description = "Starts a new game. Specify an even number of tributes and, optionally, the deadliness for the game. Use `<help>` to learn about your options.", permissions = Permissions.role(properties.getLongProperty("gameHostRoleId"))), ReactionAddListener, ReactionRemoveListener {

	private val log: Logger = LogManager.getLogger(StartGameCommand::class.java)

	private enum class StartGameState {
		NONE, THE_REAPING, STARTING
	}

	private val startGameMessages = listOf(
			"Oh how delightful! Allow me just a minute to organize the upcoming *chaos*.",
			"I can tell this is going to be a good one. Prepare yourselves, my little speed writers, while I get everything ready."
	)

	private var state = StartGameState.NONE
	private var participantCount = 24
	private var deadlinessFactor = DeadlinessFactor.NORMAL
	private var messageId: Long? = null
	private val volunteers = mutableListOf<Long>()

	override fun handleMessage(message: Message, messageContent: String) {

		val channel = api.getChannelById(properties.getLongProperty("gameChannelId")).get().asServerTextChannel().get()

		if (messageContent == "help") {
			MessageBuilder().setContent("Start a new game using this command. You may also specify the number of players (default of 24, must be even) and the deadliness. Your options for deadliness are (case insensitive):\n" +
					DeadlinessFactor.values().joinToString(", ") { it.name.toLowerCase() }).send(message.channel)
			return
		}

		if (messageContent == "cancel") {
			volunteers.clear()
			participantCount = 24
			state = StartGameState.NONE
			MessageBuilder().setContent("Cancelled starting a new game.").send(message.channel)
			return
		}

		when (state) {
			StartGameState.NONE -> {

				if (messageContent.isNotBlank()) {

					if (!parseArguments(message, messageContent)) {
						MessageBuilder().setContent("Updated the settings for the game.").send(channel)
						return
					}

				} else {
					participantCount = 24
					deadlinessFactor = DeadlinessFactor.NORMAL
				}

				val reactionEmoteId = properties.getStringProperty("volunteerReactionEmoteId")
				val reactionEmoteName = properties.getStringProperty("volunteerReactionEmoteName")

				volunteers.clear()

				thread {
					try {
						playerPreferenceService.getAll(PlayerPreferenceKey.PING_ON_START).mapValues { it.value.toBoolean() }.filter { it.value }.forEach {
							if (it.key != message.userAuthor.get().id) {
								MessageBuilder().setContent("<@${it.key}>! The Hunger Games are starting!\nIf you no longer wish to be notified, use `.unsubscribe`.").send(api.getUserById(it.key).get().openPrivateChannel().get())
							}
						}
					} catch (e: Exception) {
						log.error(e, e)
					}
				}

				val reapingMessage = MessageBuilder().setContent("The Hunger Games will begin shortly!\nIf you would like to be guaranteed a spot in these games, react to this message with <:$reactionEmoteName:$reactionEmoteId>. Run this command again to start the game.").send(channel).get()
				reapingMessage.addReaction("$reactionEmoteName:$reactionEmoteId")
				messageId = reapingMessage.id

				if (bot.activeGame != null && !bot.activeGame!!.isFinished()) {
					MessageBuilder().setContent("**!!!WARNING!!!** A game is in progress. Starting a new game will delete that one. If you're sure you want to do this, continue starting a game as you normally would. Otherwise, type `.start cancel`.").send(message.channel)
				}

				state = StartGameState.THE_REAPING

			}
			StartGameState.THE_REAPING -> {

				if (messageContent.isNotBlank()) {
					if (!parseArguments(message, messageContent)) {
						return
					} else {
						MessageBuilder().setContent("Updated the settings for the game.").send(channel)
					}
				} else {
					messageId = null
					state = StartGameState.STARTING
					MessageBuilder().setContent(startGameMessages.random()).send(channel)
					try {
						startNewGame()
					} catch (e: Exception) {
						MessageBuilder().setContent("There was an error while starting the game. It has been logged. Please notify the bot owner.").send(channel)
						log.error(e, e)
					}
					volunteers.clear()
					state = StartGameState.NONE
				}

			}
			StartGameState.STARTING -> {
				MessageBuilder().setContent("I am currently setting up a game. Please give me a moment to finish.").send(message.channel)
			}
		}

	}

	private fun parseArguments(message: Message, messageContent: String): Boolean {

		val splitMessageContents = messageContent.split(Regex("\\s+"))

		if (splitMessageContents.isNotEmpty()) {
			val messageNumberOfParticipants = splitMessageContents[0].toInt()
			if (messageNumberOfParticipants <= 0) {
				MessageBuilder().setContent("You can't run a game with no tributes, silly!").send(message.channel)
				return false
			}
			if (messageNumberOfParticipants % 2 != 0) {
				MessageBuilder().setContent("Please provide a number of participants that is a multiple of two.").send(message.channel)
				return false
			}
			participantCount = messageNumberOfParticipants
		}

		if (splitMessageContents.size >= 2) {
			val deadliness = splitMessageContents[1].toUpperCase()
			if (deadliness in DeadlinessFactor.values().map { it.name }) {
				deadlinessFactor = DeadlinessFactor.valueOf(deadliness)
			} else {
				MessageBuilder().setContent("Please provide a correct deadliness factor value (${DeadlinessFactor.values().joinToString(", ") { it.name }}).").send(message.channel)
				return false
			}
		}

		return true

	}

	private fun startNewGame() {

		val channel = api.getChannelById(properties.getLongProperty("gameChannelId")).get().asServerTextChannel().get()
		val server = channel.server

		log.info("Starting a new game")

		val users = api.getRoleById(properties.getLongProperty("tributeRoleId")).get().users.filter { !it.isYourself }.shuffled()
		val maxNumberOfTributes = (2 * floor(abs(users.size / 2.0))).toInt()

		val numberOfTributes = min(maxNumberOfTributes, participantCount)

		val tributes = mutableListOf<Tribute>()

		log.info("Found users: $users")
		log.info("Found volunteers: $volunteers")

		//All volunteers first
		tributes += volunteers.map { volunteerId ->
			users.single { it.id == volunteerId }
		}.take(numberOfTributes).map {
			createTribute(it, server)
		}

		log.info("Created volunteer tributes: $tributes")

		//If we need more, add the regular users
		//Temporarily removed so the bots have a chance. TODO Make this better later?
		/*if (tributes.size < numberOfTributes) {
			tributes += users.filter { user -> user.id !in tributes.map { it.discordId } && !user.isBot }.take(numberOfTributes - tributes.size).map {
				createTribute(it, server)
			}
		}*/

		//Finally, top it off with the bots, if necessary
		if (tributes.size < numberOfTributes) {
			tributes += users.filter { user -> user.id !in tributes.map { it.discordId } }.take(numberOfTributes - tributes.size).map {
				log.info("Creating tribute: $it")
				createTribute(it, server)
			}
		}

		//Make sure we have an even number of tributes
		if (tributes.size % 2 != 0) {
			tributes.removeLast()
		}

		log.info("Final tribute list: $tributes")

		bot.tributeStatisticsSubscriber = TributeStatisticsSubscriber()
		bot.botGameEventSubscriber = BotGameEventSubscriber(bot, channel, properties)
		bot.activeGame = HungerGame(tributes, deadlinessFactor = deadlinessFactor).apply {

			setEvents(EventLoaderService(FileService(properties), PronounService(properties), PlayerPreferenceService(properties)).loadAllEvents())
			setItems(ItemLoaderService(FileService(properties)).loadAllItems())

			addSubscriber(bot.tributeStatisticsSubscriber!!)
			addSubscriber(bot.botGameEventSubscriber!!)

		}

		try {
			bot.activeGame!!.start()
			MessageBuilder().setContent("The Hunger Games are ready. May the chaos be ever in your favor!").send(channel)
		} catch (e: IllegalStateException) {
			log.error(e, e)
			MessageBuilder().setContent("An error occurred and the hunger games could not be started.").send(channel)
		}


	}

	private fun createTribute(user: User, server: Server): Tribute {

		if (!playerPreferenceService.hasPreferenceForUser(user.id, PlayerPreferenceKey.PRONOUNS)) {

			val heRole = api.getRoleById(properties.getLongProperty("hePronounRoleId")).get()
			val sheRole = api.getRoleById(properties.getLongProperty("shePronounRoleId")).get()
			val theyRole = api.getRoleById(properties.getLongProperty("theyPronounRoleId")).get()

			when {
				heRole.hasUser(user) -> {
					playerPreferenceService.save(user.id, PlayerPreferenceKey.PRONOUNS, "he")
				}
				sheRole.hasUser(user) -> {
					playerPreferenceService.save(user.id, PlayerPreferenceKey.PRONOUNS, "she")
				}
				theyRole.hasUser(user) -> {
					playerPreferenceService.save(user.id, PlayerPreferenceKey.PRONOUNS, "they")
				}
			}

		}

		val image = ImageIO.read(ByteArrayInputStream(getRaw(user.avatar.url.toString() + "?size=256").body))
		Thread.sleep(250)

		return Tribute(user.getDisplayName(server), user.id, image)

	}

	override fun onReactionAdd(event: ReactionAddEvent) {
		event.user.ifPresent { user ->
			if (state == StartGameState.THE_REAPING && event.messageId == messageId && !user.isYourself && isVolunteerEmote(event.emoji)) {
				val usersWithRole = api.getRoleById(properties.getLongProperty("tributeRoleId")).get().users
				if (usersWithRole.none { it.id == user.id }) {
					MessageBuilder().setContent("<@${user.id}> You need to have the tribute role to volunteer.").send(event.channel)
				} else {
					log.debug("${user.discriminatedName} (${user.id}) has volunteered.")
					volunteers += user.id
				}
			}
		}
	}

	override fun onReactionRemove(event: ReactionRemoveEvent) {
		event.user.ifPresent { user ->
			if (state == StartGameState.THE_REAPING && event.messageId == messageId && !user.isYourself && isVolunteerEmote(event.emoji) && user.id in volunteers) {
				log.debug("${user.discriminatedName} (${user.id}) has un-volunteered.")
				volunteers -= user.id
			}
		}
	}

	private fun isVolunteerEmote(emoji: Emoji): Boolean {
		if (emoji.isCustomEmoji) {
			val emote = emoji.asCustomEmoji().get()
			return emote.name == properties.getStringProperty("volunteerReactionEmoteName") && emote.id == properties.getLongProperty("volunteerReactionEmoteId")
		}
		return false
	}

}
