package net.krazyweb.hungerbot.commands.game

import net.krazyweb.hungerbot.commands.Command
import net.krazyweb.hungergames.data.PlayerPreferenceKey
import net.krazyweb.hungergames.services.PlayerPreferenceService
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder

class SubscribeCommand(private val preferenceService: PlayerPreferenceService) : Command("subscribe", description = "Subscribe to game start notifications. The bot will message you when a game has been started.") {

	val log: Logger = LogManager.getLogger(SubscribeCommand::class.java)

	override fun handleMessage(message: Message, messageContent: String) {

		message.userAuthor.ifPresent { user ->
			preferenceService.save(user.id, PlayerPreferenceKey.PING_ON_START, "true")
			MessageBuilder().setContent("${getReplyMention(message)} Got it. I'll let you know when the next game starts!").send(message.channel)
		}

	}

}
