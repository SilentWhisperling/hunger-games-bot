package net.krazyweb.hungerbot.commands.game

import net.krazyweb.hungerbot.HungerGamesBot
import net.krazyweb.hungerbot.Permissions
import net.krazyweb.hungerbot.commands.Command
import net.krazyweb.hungerbot.util.EmbedHelper
import net.krazyweb.util.BotProperties
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder

class LeaderboardCommand(private val bot: HungerGamesBot, val botProperties: BotProperties) : Command("leaderboard", "standings", "statistics", "rank", "ranks", "stats", description = "Shows the leaderboard for the current game.", permissions = Permissions.role(botProperties.getLongProperty("gameHostRoleId"))) {

	override fun handleMessage(message: Message, messageContent: String) {

		if (bot.activeGame == null) {
			MessageBuilder().setContent("There is no game in progress. Please start a new game before trying to view the leaderboard.").send(message.channel)
			return
		}

		EmbedHelper.sendLeaderboardMessages(bot, message.channel)

	}

}
