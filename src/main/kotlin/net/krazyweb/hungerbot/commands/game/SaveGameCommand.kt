package net.krazyweb.hungerbot.commands.game

import net.krazyweb.hungerbot.HungerGamesBot
import net.krazyweb.hungerbot.Permissions
import net.krazyweb.hungerbot.commands.Command
import net.krazyweb.hungergames.services.*
import net.krazyweb.util.BotProperties
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter

class SaveGameCommand(private val bot: HungerGamesBot, private val botProperties: BotProperties) : Command("save", description = "Saves the current game to a .zip file.", permissions = Permissions.role(botProperties.getLongProperty("gameHostRoleId"))) {

	override fun handleMessage(message: Message, messageContent: String) {

		if (bot.activeGame == null) {
			MessageBuilder().setContent("There is no game in progress. Cannot save a game when there is not one to save.").send(message.channel)
			return
		}

		val dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss").withZone(ZoneId.systemDefault())
		val time = dateFormat.format(Instant.now())

		val saveService = GamePersistenceService(FileService(botProperties), EventLoaderService(FileService(botProperties), PronounService(botProperties), PlayerPreferenceService(botProperties)), ItemLoaderService(FileService(botProperties)))
		MessageBuilder().addAttachment(saveService.save(bot.activeGame!!), "game_$time.zip").send(message.channel)

	}

}
