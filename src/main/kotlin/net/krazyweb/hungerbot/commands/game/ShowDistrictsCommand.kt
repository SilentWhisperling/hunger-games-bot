package net.krazyweb.hungerbot.commands.game

import net.krazyweb.hungerbot.HungerGamesBot
import net.krazyweb.hungerbot.Permissions
import net.krazyweb.hungerbot.commands.Command
import net.krazyweb.util.BotProperties
import net.krazyweb.util.ImageUtils
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder
import org.javacord.api.entity.message.embed.EmbedBuilder
import java.awt.Color

class ShowDistrictsCommand(private val bot: HungerGamesBot, val botProperties: BotProperties) : Command("teams", "districts", description = "Displays an image containing all tributes/districts in the current game and their statuses.", permissions = Permissions.role(botProperties.getLongProperty("gameHostRoleId"))) {

	override fun handleMessage(message: Message, messageContent: String) {

		if (bot.activeGame == null) {
			MessageBuilder().setContent("There is no game in progress. Please start a new game before trying to view the teams.").send(message.channel)
			return
		}

		MessageBuilder().setEmbed(EmbedBuilder()
				.setColor(Color(0.3f, 0.6f, 0.9f, 1.0f))
				.setImage(ImageUtils.createScoreboard(bot.activeGame!!.getDistricts(), bot.tributeStatisticsSubscriber!!))
		).send(message.channel)

	}

}
