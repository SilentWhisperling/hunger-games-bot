package net.krazyweb.hungerbot

import org.javacord.api.entity.message.Message


class Permissions {

	companion object {

		fun role(vararg roles: Long): (message: Message) -> Boolean {

			return { message ->
				roles.map { message.api.getRoleById(it).get() }.any {
					it.hasUser(message.author.asUser().get())
				}
			}

		}

		fun userId(userId: Long): (message: Message) -> Boolean {

			return { message ->
				message.author.asUser().get().id == userId
			}

		}

	}

}
