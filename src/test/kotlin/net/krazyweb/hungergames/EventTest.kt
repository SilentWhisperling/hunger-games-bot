package net.krazyweb.hungergames

import io.mockk.every
import net.krazyweb.hungergames.data.Tribute
import net.krazyweb.hungergames.data.events.Event
import net.krazyweb.hungergames.game.ArenaData
import net.krazyweb.hungergames.services.EventLoaderService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class EventTest : BotTest() {

	@Test
	fun `canRunEvent with only a message`() {

		val event = createEvent("""
			[{
				"message": "1"
			}]
		""")

		testEvent(event) { tributes, fatal, itemEvent, _, _, activeEvent ->
			tributes.size == 1 && !fatal && !itemEvent && activeEvent == null
		}

	}

	@Test
	fun `canRunEvent with a fatality`() {

		val event = createEvent("""
			[{
				"message": "1",
				"killed": [1],
				"deathType": "environmental"
			}]
		""")

		testEvent(event) { tributes, fatal, itemEvent, _, _, activeEvent ->
			tributes.size == 1 && fatal && !itemEvent && activeEvent == null
		}

	}

	@Test
	fun `canRunEvent with a killer and a fatality`() {

		val event = createEvent("""
			[{
				"message": "1 2",
				"killers": [1],
				"killed": [2],
				"deathType": "murder"
			}]
		""")

		testEvent(event) { tributes, fatal, itemEvent, _, _, activeEvent ->
			tributes.size == 2 && fatal && !itemEvent && activeEvent == null
		}

	}

	@Test
	fun `canRunEvent requiring an arena event that is not fatal`() {

		val event = createEvent("""
			[{
				"message": "1 2",
				"event": "The Bloodbath"
			}]
		""")

		testEvent(event) { tributes, fatal, itemEvent, _, _, activeEvent ->
			tributes.size == 2 && !fatal && !itemEvent && activeEvent == "The Bloodbath"
		}

	}

	@Test
	fun `canRunEvent requiring an arena event`() {

		val event = createEvent("""
			[{
				"message": "1 2",
				"event": "The Bloodbath",
				"killers": 1,
				"killed": 2,
				"deathType": "murder"
			}]
		""")

		testEvent(event) { tributes, fatal, itemEvent, _, _, activeEvent ->
			tributes.size == 2 && fatal && !itemEvent && activeEvent == "The Bloodbath"
		}

	}

	@Test
	fun `canRunEvent requiring the day`() {

		val event = createEvent("""
			[{
				"message": "1 2 3 4 5",
				"timeOfDay": "day"
			}]
		""")

		testEvent(event) { tributes, fatal, itemEvent, _, time, activeEvent ->
			tributes.size == 5 && !fatal && !itemEvent && time == "day" && activeEvent == null
		}

	}

	@Test
	fun `canRunEvent requiring the night`() {

		val event = createEvent("""
			[{
				"message": "1",
				"timeOfDay": "night"
			}]
		""")

		testEvent(event) { tributes, fatal, itemEvent, _, time, activeEvent ->
			tributes.size == 1 && !fatal && !itemEvent && time == "night" && activeEvent == null
		}

	}

	@Test
	fun `canRunEvent requiring ten tributes`() {

		val event = createEvent("""
			[{
				"message": "1 2 3 4 5 6 7 8 9 10"
			}]
		""")

		testEvent(event) { tributes, fatal, itemEvent, _, _, activeEvent ->
			tributes.size == 10 && !fatal && !itemEvent && activeEvent == null
		}

	}

	@Test
	fun `canRunEvent requiring an item`() {

		val event = createEvent("""
			[{
				"message": "1",
				"items": [
					{
						"id": 1,
						"owner": 1,
						"tags": ["any"]
					}
				]
			}]
		""")

		testEvent(event) { tributes, fatal, itemEvent, _, _, activeEvent ->
			tributes.size == 10 && !fatal && itemEvent && activeEvent == null
		}

	}

	@Test
	fun `Event kills a tribute with no killer`() {

		val event = createEvent("""
			[{
				"message": "1",
				"killed": 1,
				"deathType": "environmental"
			}]
		""")

		val tribute = getTributes().subList(0, 1)

		event.runEvent(tribute)

		Assertions.assertFalse { tribute[0].alive }

	}

	@Test
	fun `Event kills a tribute with a killer`() {

		val event = createEvent("""
			[{
				"message": "1 2",
				"killed": 2,
				"killers": 1,
				"deathType": "murder"
			}]
		""")

		val tributes = getTributes().subList(0, 2)

		event.runEvent(tributes)

		Assertions.assertTrue { tributes[0].alive }
		Assertions.assertFalse { tributes[1].alive }

	}

	@Test
	fun `Event kills the correct tributes with lots of them mixed in`() {

		val event = createEvent("""
			[{
				"message": "1 2 3 4 5 6 7 8 9 10",
				"killed": [1, 3, 4, 6, 7, 9],
				"killers": [2, 5, 8, 10],
				"deathType": "murder"
			}]
		""")

		val tributes = getTributes().subList(0, 10)

		event.runEvent(tributes)

		Assertions.assertFalse { tributes[0].alive }
		Assertions.assertTrue { tributes[1].alive }
		Assertions.assertFalse { tributes[2].alive }
		Assertions.assertFalse { tributes[3].alive }
		Assertions.assertTrue { tributes[4].alive }
		Assertions.assertFalse { tributes[5].alive }
		Assertions.assertFalse { tributes[6].alive }
		Assertions.assertTrue { tributes[7].alive }
		Assertions.assertFalse { tributes[8].alive }
		Assertions.assertTrue { tributes[9].alive }

	}

	@Test
	fun `Event kills the correct tributes when some don't appear in either killed or killers`() {

		val event = createEvent("""
			[{
				"message": "1 2 3 4 5",
				"killed": [1, 3],
				"killers": [5],
				"deathType": "murder"
			}]
		""")

		val tributes = getTributes().subList(0, 10)

		event.runEvent(tributes)

		Assertions.assertFalse { tributes[0].alive }
		Assertions.assertTrue { tributes[1].alive }
		Assertions.assertFalse { tributes[2].alive }
		Assertions.assertTrue { tributes[3].alive }
		Assertions.assertTrue { tributes[4].alive }

	}

	//TODO Event adds items
	//TODO Event removes items
	//TODO Event moves items to another tribute
	//TODO Event correctly checks items requirements

	private fun testEvent(event: Event, predicate: (tributes: List<Tribute>, fatal: Boolean, itemEvent: Boolean, day: Int, time: String, activeEvent: String?) -> Boolean) {
		return testEvent(getTributes(), event, predicate)
	}

	private fun testEvent(allTributes: List<Tribute>, event: Event, predicate: (tributes: List<Tribute>, fatal: Boolean, itemEvent: Boolean, day: Int, time: String, activeEvent: String?) -> Boolean) {

		for (i in 0 until 10) {

			val tributes = allTributes.subList(0, i)

			for (time in listOf("day", "night")) {
				for (activeEvent in listOf(null, "The Bloodbath", "Hurricane")) {
					for (day in 1..10) {
						for (fatal in listOf(true, false)) {
							for (itemEvent in listOf(true, false)) {

								if (predicate(tributes, fatal, itemEvent, day, time, activeEvent)) {
									assertTrue(event.canRunEvent(tributes, fatal, itemEvent, ArenaData(day, time, activeEvent)))
								} else {
									assertFalse(event.canRunEvent(tributes, fatal, itemEvent, ArenaData(day, time, activeEvent)))
								}

							}
						}
					}
				}
			}

		}

	}

	private fun createEvent(event: String): Event {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf(event.trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		return eventLoaderService.loadAllEvents().first()

	}

}
