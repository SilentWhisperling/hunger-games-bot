package net.krazyweb.hungergames

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.mockk.every
import io.mockk.mockk
import net.krazyweb.hungergames.data.Conjugation
import net.krazyweb.hungergames.data.PlayerPreferenceKey
import net.krazyweb.hungergames.data.Pronouns
import net.krazyweb.hungergames.data.Tribute
import net.krazyweb.hungergames.services.FileService
import net.krazyweb.hungergames.services.PlayerPreferenceService
import net.krazyweb.hungergames.services.PronounService
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.awt.image.BufferedImage
import java.nio.file.*
import java.nio.file.attribute.BasicFileAttributes

abstract class BotTest {

	private val log: Logger = LogManager.getLogger(BotTest::class.java)

	private data class TestTribute(val name: String, val id: Long, val pronouns: String)

	fun readTestResource(path: String): String {

		return BotTest::class.java.classLoader.getResourceAsStream(path).use {
			if (it == null) {
				throw IllegalArgumentException("Could not locate resource $path")
			}
			String(it.readAllBytes())
		}

	}

	fun getTributes(): List<Tribute> {

		val testTributes: List<TestTribute> = jacksonObjectMapper().readValue(readTestResource("tributes.json"), object : TypeReference<List<TestTribute>>(){})

		return testTributes.map { Tribute(it.name, it.id, BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB)) }

	}

	fun copyTestEventsToFolder(directory: Path) {

		val outputDir = directory.resolve("data").resolve("events")
		Files.createDirectories(outputDir)

		val eventsDir = Paths.get(BotTest::class.java.classLoader.getResource("")!!.toURI()).resolve("events")

		log.debug("Copying event resources from $eventsDir to $outputDir")

		Files.walkFileTree(eventsDir, object : SimpleFileVisitor<Path>() {

			override fun preVisitDirectory(dir: Path, attrs: BasicFileAttributes?): FileVisitResult {
				log.debug("Creating ${outputDir.resolve(eventsDir.relativize(dir))}")
				Files.createDirectories(outputDir.resolve(eventsDir.relativize(dir)))
				return FileVisitResult.CONTINUE
			}

			override fun visitFile(file: Path, attrs: BasicFileAttributes?): FileVisitResult {
				log.debug("Copying ${outputDir.resolve(eventsDir.relativize(file))}")
				Files.copy(file, outputDir.resolve(eventsDir.relativize(file)))
				return FileVisitResult.CONTINUE
			}

		})


	}

	fun getMockPlayerPreferenceService(): PlayerPreferenceService {

		val playerPreferenceService = mockk<PlayerPreferenceService>()
		val testTributes: List<TestTribute> = jacksonObjectMapper().readValue(readTestResource("tributes.json"), object : TypeReference<List<TestTribute>>(){})

		testTributes.forEach {
			every { playerPreferenceService.get(it.id, PlayerPreferenceKey.PRONOUNS) } returns it.pronouns
		}

		return playerPreferenceService

	}

	fun getMockPronounService(): PronounService {

		val pronounService = mockk<PronounService>()
		every { pronounService.getPronouns("he") } returns Pronouns("he", "him", "his", "his", "himself", Conjugation.SINGULAR)
		every { pronounService.getPronouns("she") } returns Pronouns("she", "her", "her", "hers", "herself", Conjugation.SINGULAR)
		every { pronounService.getPronouns("they") } returns Pronouns("they", "them", "their", "theirs", "themselves", Conjugation.PLURAL)

		return pronounService

	}

	fun getMockFileService(): FileService {
		val fileService = mockk<FileService>()
		every { fileService.readAllFromDirectory("data", "events") } returns emptyList()
		every { fileService.readAllFromDirectory("data", "events", "day") } returns emptyList()
		every { fileService.readAllFromDirectory("data", "events", "night") } returns emptyList()
		every { fileService.readAllFromDirectory("data", "events", "arena") } returns emptyList()
		return fileService
	}

}
