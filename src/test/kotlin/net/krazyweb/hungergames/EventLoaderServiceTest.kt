package net.krazyweb.hungergames

import io.mockk.every
import io.mockk.mockk
import net.krazyweb.hungergames.data.DeathType
import net.krazyweb.hungergames.services.EventFormatException
import net.krazyweb.hungergames.services.EventLoaderService
import net.krazyweb.hungergames.services.FileService
import net.krazyweb.util.BotProperties
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.test.assertEquals
import kotlin.test.assertNull
import kotlin.test.assertTrue

class EventLoaderServiceTest : BotTest() {

	private val log: Logger = LogManager.getLogger(EventLoaderServiceTest::class.java)

	@Test
	fun `Event service sanity test`(@TempDir directory: Path) {

		copyTestEventsToFolder(directory)

		val properties = getMockedProperties(directory)
		val eventLoaderService = EventLoaderService(FileService(properties), getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		log.info("Loaded ${events.size} events from test directory")

		assertTrue { events.isNotEmpty() }

	}

	@Test
	fun `Event service loads all production data successfully`() {

		val properties = getMockedProperties(Paths.get(""))
		val eventLoaderService = EventLoaderService(FileService(properties), getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		log.info("Loaded ${events.size} events from production data directory")

		assertTrue { events.isNotEmpty() }

	}

	@Test
	fun `Event service loads a default event`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals("any", events.first().timeOfDay)
		assertEquals(listOf(), events.first().killers)
		assertEquals(listOf(), events.first().killed)
		assertEquals(listOf(), events.first().itemReferences)
		assertEquals(listOf(), events.first().inventoryChanges)
		assertNull(events.first().event)

	}

	@Test
	fun `Event service time of day loads correct values`(@TempDir directory: Path) {

		val fileService = getMockFileService()

		listOf("day", "night", "any").forEach {

			every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
				[{
					"message": "1",
					"timeOfDay": "$it"
				}]
			""".trimIndent())

			val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
			val events = eventLoaderService.loadAllEvents()

			assertEquals(it, events.first().timeOfDay)

		}

	}

	@Test
	fun `Event service loads time of day from field`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1",
				"timeOfDay": "day"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals("day", events.first().timeOfDay)

	}

	@Test
	fun `Event service loads time of day from directory`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events", "day") } returns listOf("""
			[{
				"message": "1"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals("day", events.first().timeOfDay)

	}

	@Test
	fun `Event service time of day from directory is overridden by field`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events", "day") } returns listOf("""
			[{
				"message": "1",
				"timeOfDay": "night"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals("night", events.first().timeOfDay)

	}

	@Test
	fun `Event service loads arena event values`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events", "arena") } returns listOf("""
			[{
				"message": "1",
				"event": "The Bloodbath"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals("The Bloodbath", events.first().event)

	}

	@Test
	fun `Event service loads single killer values`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1",
				"killers": 1,
				"killed": 1,
				"deathType": "murder"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals(listOf(0), events.first().killers)

	}

	@Test
	fun `Event service loads array of killer values`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1 2",
				"killers": [1, 2],
				"killed": 1,
				"deathType": "murder"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals(listOf(0, 1), events.first().killers)

	}

	@Test
	fun `Event service loads single killed values`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1",
				"killed": 1,
				"deathType": "environmental"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals(listOf(0), events.first().killed)

	}

	@Test
	fun `Event service loads array of killed values`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1 2",
				"killed": [1, 2],
				"deathType": "environmental"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals(listOf(0, 1), events.first().killed)

	}

	@Test
	fun `Event service requires killed when killer present`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1 2",
				"killers": 1,
				"deathType": "murder"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		assertThrows(EventFormatException::class.java) { eventLoaderService.loadAllEvents() }

	}

	@Test
	fun `Event service loads deathTypes`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1 2",
				"killers": [1, 2],
				"killed": 1,
				"deathType": "murder"
			},{
				"message": "1 2",
				"killers": [1, 2],
				"killed": 1,
				"deathType": "accidental"
			},{
				"message": "1 2"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals(DeathType.MURDER, events.first().deathType)
		assertEquals(DeathType.ACCIDENTAL, events[1].deathType)
		assertNull(events[2].deathType)

	}

	//TODO Event service loads item stuff
	//TODO Event service fails to load if there's not items for every tag
	//TODO Event service loads inventory change stuff

	private fun getMockedProperties(directory: Path): BotProperties {

		val properties = mockk<BotProperties>()
		every { properties.getPathProperty("workingDirectory") } returns directory

		return properties

	}

}
