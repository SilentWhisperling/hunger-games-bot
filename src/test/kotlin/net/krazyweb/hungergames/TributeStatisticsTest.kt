package net.krazyweb.hungergames

import io.mockk.every
import io.mockk.mockk
import net.krazyweb.hungergames.data.messages.FatalEventMessage
import net.krazyweb.hungergames.data.messages.GameOverMessage
import net.krazyweb.hungergames.data.messages.Message
import net.krazyweb.hungergames.data.messages.Subscriber
import net.krazyweb.hungergames.game.DeadlinessFactor
import net.krazyweb.hungergames.game.HungerGame
import net.krazyweb.hungergames.game.StatisticsKey
import net.krazyweb.hungergames.game.TributeStatisticsSubscriber
import net.krazyweb.hungergames.services.EventLoaderService
import net.krazyweb.hungergames.services.FileService
import net.krazyweb.hungergames.services.ItemLoaderService
import net.krazyweb.util.BotProperties
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.api.RepetitionInfo
import java.nio.file.Paths
import kotlin.math.abs
import kotlin.math.floor
import kotlin.math.min
import kotlin.test.assertEquals

class TributeStatisticsTest : BotTest() {

	private val log: Logger = LogManager.getLogger(TributeStatisticsTest::class.java)

	//TODO Find a better way to test this that doesn't involve just having enough runs to probably catch a few kills. Maybe track all the kills for each tribute and compare?
	@RepeatedTest(200)
	fun `Tribute kills are tracked correctly`(info: RepetitionInfo) {

		val game = getAGame()

		var gameFinished = false

		val tributeStatisticsSubscriber = TributeStatisticsSubscriber()

		val tribute = game.tributes[0]
		var tributeKills = 0

		game.addSubscriber(tributeStatisticsSubscriber)
		game.addSubscriber(object : Subscriber {
			override fun call(message: Message) {
				when (message) {
					is FatalEventMessage -> {
						log.debug(message.killers)
						if (message.killers.any { it.discordId == tribute.discordId }) {
							tributeKills += message.defeatedTributes.filter { it.discordId != tribute.discordId }.size
						}
					}
					is GameOverMessage -> {
						gameFinished = true
					}
				}
			}
		})

		game.start()

		while (!gameFinished) {
			game.next()
		}

		log.debug("$tributeKills kills for ${tribute.name}")

		assertEquals(tributeKills, tributeStatisticsSubscriber.getStatistics(tribute.discordId).get(StatisticsKey.KILLS, 0))

	}

	private fun getAGame(seed: Long = System.currentTimeMillis()): HungerGame {

		val properties = getMockProperties()
		val tributes = getTributes()
		val events = EventLoaderService(FileService(properties), getMockPronounService(), getMockPlayerPreferenceService()).loadAllEvents()
		val items = ItemLoaderService(FileService(properties)).loadAllItems()

		val numberOfTributes = min(24, (2 * floor(abs(tributes.size / 2.0))).toInt())

		val game = HungerGame(tributes.take(numberOfTributes), seed, deadlinessFactor = DeadlinessFactor.TWI_PLS_NO)
		game.setEvents(events)
		game.setItems(items)

		return game

	}

	private fun getMockProperties(): BotProperties {
		val properties = mockk<BotProperties>()
		every { properties.getPathProperty("workingDirectory") } returns Paths.get("")
		return properties
	}

}
